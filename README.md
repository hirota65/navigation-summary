# navigation summary
## 目次

1. Navigation_system の全体図
2. Navigationパッケージ変更箇所
 - Map_serverを一部変更
   - 変更箇所
   - 変更前の課題
   - 変更後の改善点
 - Costmapを一部変更
   - 変更箇所
   - 変更前の課題
   - 変更後の改善点
3. LocalplannerをTEB_local_plannerにした理由
4. ラベル指定navigation実験の処理の流れ
5. ピクセル座標を実空間座標に変換する方法
6. 作成したプログラムの内容


## 1.Navigation_system の全体図

![Navigation_system の全体図](navigation_全体図.png)
> http://wiki.ros.org/move_base

## 2.Navigationパッケージ変更箇所

### Map_serverを一部変更

- 変更箇所((find navigation)/map_server/src/ main_hirota.cpp, map_saver_hirota.cpp)

 - マップを更新しながらロボットが進めるようにした。
- 変更前の課題
  - マップを作成してあることがnavigationの前提条件だったので、実験前にいちいちマップを作成しておかねばならなかった。
  - 作成したマップは実験中、更新されないのでマップ上の未観測領域には絶対行けなかった。


- 変更後の改善点
  - マップを作成しておかなくても、navigationが起動できるようになった。
  - マップを更新できるので、SLAMで観測すれば未観測領域をNavigation可能な領域に更新していけるようになった。

### Costmapを一部変更
  - 変更箇所(catkin_ws/src/turtlebot3/turtlebot3_navigation/param/teb/costmap_common_params.yaml)
    - Local_costmapとGlobal_costmapの共通設定ファイルcostmap_common_params.yamlのinflation_radius を0.5から0.1に変更
    - 障害物からの距離に応じたコストを０に打ち切る閾値を小さくした。
  - 変更前の課題
    - 以前の設定では障害物の周りにもコストがあったため、狭い領域を通る経路を作成することが困難になっていた。

  - 変更後の改善点
    - 障害物の間などの狭い領域をロボットが通れるようになった。

## 3.LocalplannerをTEB_local_plannerにした理由

- Turtlebot_navigationではもともとDWA_local_plannerが標準で使われていたが、ゴールに到達する予備動作が必要以上に大きく、時間がかかるので、予備動作の少ないTEB_local_plannerに変更した。

## 4. ラベル指定navigation実験の処理の流れ

![ラベル指定navigation実験の処理の流れ](実験処理.png)

## 5.ピクセル座標を実空間座標に変換する方法

1. カメラのピクセル数と視野角から仮想的なカメラと画像の距離Dを求める。

 - D_x =(横のピクセル数/2) /(tan(水平視野角/2))
 - D_y =(縦のピクセル数/2) /(tan(垂直視野角/2))

> ここでは水平視野角70度、垂直視野角48度,
 横のピクセル数1280,縦のピクセル数720と入力

> 正確な水平視野角、垂直視野角はまだ調べてないので、早急に調べたい


![方法1_0](方法1_0.png)

> ※本来はD_x,D_yは等しいはずだが、視野角が随意で
はなく、こちらの都合で調整したため、別々の
値になっている。もし、不都合であれば、
D_x,D_yを一致させるように水平視野角と
垂直視野角を調整するとよいかもしれない。


2. カメラと画像の距離（D_x, D_y)とピクセル座標を使って、中心軸からの角度を算出する。

 - θx= arctan(point_x(ピクセル座標のx成分)/D_x)
 - θy= arctan(point_y(ピクセル座標のy成分)/D_y)

![方法1_1](方法1_4.png)
> 正面上から見た図

![方法1_1](方法1_5.png)
> 真横から見た図


3. 中心軸からの角度とカメラの高さと姿勢角から実空間座標を算出する。

![方法1_1](方法1_1.png)

![方法1_1](方法1_3.png)
> 横から見た図

- A = カメラの高さ/cos(カメラの角度-θy)
- y = カメラの高さ×tan(カメラの角度-θy)

![方法1_1](方法1_2.png)
> 上から見た図

x = A＊tan(θx) 　　よって
- x=カメラの高さ×tan(θx)/cos(カメラの角度-θy)

### 座標変換処理の流れ

![座標変換処理](座標変換処理.png)

## 6.作成したプログラムの内容

### send_goal_calc/src/

- send_goal_area_2_new.cpp

  - 領域指定でmove_baseにgoal座標を送信する

- send_goal_pixel.cpp

  - 座標変換（ピクセル座標->実空間座標)領域指定でmove_baseにgoal座標を送信する

- send_goal_area_pub_new.cpp

  - send_goal_area_2_new.cppをテストするためのプログラム

  - 疑似的なquery_graphとtarget_graphをパブリッシュする

- send_goal_pixel_pub.cpp

  - send_goal_pixel.cppをテストするためのプログラム

  - 疑似的なquery_graphとtarget_graphをパブリッシュする

### mapping_pix_to_position/src/

- mapping_using_imu.cpp
  - YOLOのBoundingBoxとimuのtf情報を受け取り、実空間座標の情報が入ったmoverio::geoBoundingBoxesをパブリッシュする
